#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from gi.repository import Gdk

import codecs
import re
import os
import sys
import unicodedata
from xml.dom import minidom

import booklst
import hipconv
import configparser
from textview import Show_text
import hipcomment
import russ_simp
import gui_conf

#conv = hipconv.Repl()
#brac = hipcomment.Brackets()

# russify text in Viewer
russ = russ_simp.Mn(True, True)

from betacode import Beta
bcode = Beta()

def destroy_cb(widget):
    Gtk.main_quit()
    return False

class Popup:
    def __init__(self, text=None):
#        self.title = ""
        self.text = text
        dialog = Gtk.Dialog(title='warning')
        label = Gtk.Label()
        label.set_text(self.text)
        dialog.vbox.pack_start(label, True, True, 10)
        label.show()
        dialog.show()

class SeText(Show_text):

    ''' Subclass of Text(), viewer of texts
    with some special features, needed for hipsearch

    '''

    def __init__(self):
        Show_text.__init__(self, False)
        self.textview.connect('move-cursor', self.scrolled)
        self.scr = 0

    def scrolled(self, textv, steps, count, ext):
       self.scr = steps 
#       print(count, ext)
       
    def style_txt(self):
        # Have to change only this method, everything else is from hipview
        self.my_text.set_property("font", self.sl_font)
        self.my_text.set_property("wrap-mode", Gtk.WrapMode(2)) # wrap word
        
        startiter, enditer = self.textbuffer.get_bounds()
        self.textbuffer.apply_tag(self.my_text, startiter, enditer)
        self.textview.set_editable(False)
        cur_iter = self.textbuffer.get_iter_at_line(self.pos)

#        if not self.pos:
#            cur_iter = self.textbuffer.get_iter_at_line(self.pos)
#        else:
#            # put cursor at the start of the page
#            self.textbuffer.place_cursor(startiter)
#            cur_iter = startiter

        print("найденный ряд", face.new_win.found_raw)

        iter1 = self.textbuffer.get_iter_at_line_offset(face.new_win.found_raw, 0)
        iter2 = self.textbuffer.get_iter_at_line_offset(face.new_win.found_raw + 1, 0)

        self.found_text = Gtk.TextTag()
        self.found_text.set_property("background", "lightgreen")
        self.tag_table.add(self.found_text)
        self.textbuffer.apply_tag(self.found_text, iter1, iter2)

#        if isinstance(widget, Gtk.TreeView):

        if not self.scr:
            self.textbuffer.place_cursor(iter1)
            one = self.textbuffer.create_mark(None, iter1)
#            self.textview.scroll_to_mark(one, 0.0, True, 0.0, 0.0)
            self.textview.scroll_to_iter(iter1, 0.0, True, 0.0, 0.0)
            self.textview.grab_focus()
        else:
            self.textbuffer.place_cursor(cur_iter)
            cur_mark = self.textbuffer.create_mark(None, cur_iter)
#            self.textview.scroll_to_mark(cur_mark, 0.0, True, 0.0, 0.0)
            self.textview.scroll_to_iter(iter1, 0.0, True, 0.0, 0.0)
            self.textview.grab_focus()
#            print('scrolled', self.scr)

        self.scr = 0

    def style_txt_gr(self):
        # Have to change only this method, everything else is from hipview
        self.my_text_gr.set_property("font", self.gr_font)
#        self.my_text_gr.set_property("wrap-mode", Gtk.WRAP_WORD) # see also str #68
        self.my_text_gr.set_property("wrap-mode", Gtk.WrapMode(2)) # wrap word
        
        startiter, enditer = self.textbuffer.get_bounds()
        self.textbuffer.apply_tag(self.my_text_gr, startiter, enditer)

        cur_iter = self.textbuffer.get_iter_at_line(self.pos)

#        if not self.pos:
#            cur_iter = self.textbuffer.get_iter_at_line(self.pos)
#        else:
#            # put cursor at the start of the page
#            self.textbuffer.place_cursor(startiter)
#            cur_iter = startiter

        iter1 = self.textbuffer.get_iter_at_line_offset(face.new_win.found_raw, 0)
        iter2 = self.textbuffer.get_iter_at_line_offset(face.new_win.found_raw + 1, 0)

        self.found_text = Gtk.TextTag()
        self.found_text.set_property("background", "lightgreen")
        self.tag_table_gr.add(self.found_text)
        self.textbuffer.apply_tag(self.found_text, iter1, iter2)

#        if isinstance(widget, Gtk.TreeView):

        if not self.scr:
            self.textbuffer.place_cursor(iter1)
            one = self.textbuffer.create_mark(None, iter1)
            self.textview.scroll_to_mark(one, 0.0, True, 0.0, 0.0)
#            self.textview.scroll_to_iter(iter1, 0.0, True, 0.0, 0.0)
            self.textview.grab_focus()
        else:
            self.textbuffer.place_cursor(cur_iter)
            cur_mark = self.textbuffer.create_mark(None, cur_iter)
            self.textview.scroll_to_mark(cur_mark, 0.0, True, 0.0, 0.0)
            self.textview.grab_focus()
#            print('scrolled', self.scr)

        self.scr = 0

class Viewer:
    ''' List of found entries

    '''

    def __init__(self):
        self.res_num = 0
        self.text = 'Searching...'
        window2 = Gtk.Window()
        window2.set_resizable(True)
        window2.set_size_request(950, 450)

        window2.set_title("Просмотр результатов")
        window2.set_border_width(0)
#        window2.connect('key_press_event', on_key_press_event)

        box1 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=0)
        window2.add(box1)
        box1.show()
#        box2 = Gtk.VBox(False, 10)
        box2 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        box2.set_border_width(10)
        box1.pack_start(box2, True, True, 0)
        box2.show()

        sw = Gtk.ScrolledWindow()
        sw.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)  # ALWAYS, NEVER.
        self.model = Gtk.ListStore(str, int, str, str)
        self.tv = Gtk.TreeView(model=self.model)
        self.selection = self.tv.get_selection()

#        self.tv.connect('key_press_event', self.on_key_press_event)
        self.tv.connect('row-activated', self.on_click)

        self.modelfilter = self.model.filter_new()
#        self.modelfilter.set_visible_func(vis, self.small)
        self.tv.set_model(self.modelfilter)

        sw.add(self.tv)

        self.label = Gtk.Label() 
        self.entry = Gtk.Entry()
        
        cell1 = Gtk.CellRendererText()
        cell2 = Gtk.CellRendererText()

        # font to render found entries in Viewer is aquired from config
        cell1.set_property('font', face.config.get('Fonts', 'reg_font'))
        cell2.set_property('font', face.config.get('Fonts', 'reg_font'))

        self.column = Gtk.TreeViewColumn("Путь", cell1, text=3)
        self.column2 = Gtk.TreeViewColumn("Строка", cell2, text=2)
        self.tv.append_column(self.column)
        self.tv.append_column(self.column2)
        self.tv.set_search_column(2)
        
        sw.show_all()
        self.tv.show()
        box2.pack_start(self.label, False, False, 0)
        box2.pack_start(sw, True, True, 0)
        box2.pack_start(self.entry, False, False, 0)

        self.entry.connect('activate', self.entry_cb)
        self.entry.connect('key_press_event', self.on_key_press_event)
        self.label.set_text("Найдено " + str(count) + " совпадений")
        self.entry.show()
        self.label.show()

        window2.show()

    def entry_cb(self, ent):
        """Callback for entry widg
           Look for word in results of primary search

        """
        
        global search_res
        global res_num

        res_num = 0
#    search_res = []

        col_num = 2
        search = ent.get_text()

        data = [search]

        def find_match(model, path, iter, data):

            match_line = model.get_value(iter, col_num)
            if match_line.find(data[0]) >= 0:
                data.append(path)

        face.new_win.modelfilter.foreach(find_match, data)
        search_res = data[1:]
        if search_res:
            face.new_win.selection.select_path(search_res[0])
            face.new_win.tv.scroll_to_cell(search_res[0])
        else:
            print('no match found')


    def ins(self, res):

        for i in res:
            iter = self.model.append()
            short_path = i[0].split("hiptools")
            self.model.set(iter, 0, i[0])
            # 0 - колонка в модели, куда вставляется i[0]. На 0 может ссылаться колонка в ListStore (сейчас -- на 3 и 2)
            self.model.set(iter, 1, i[1])
            self.model.set(iter, 2, i[2][:40])
#            path_s_u = face.path2name(i[0])[:40]
#            self.model.set(iter, 3, path_s_u)
#            self.model.set(iter, 3, i[0][:40])
            self.model.set(iter, 3, short_path[1])

    def on_click(self, widget, iter, path):
        ''' callback, row in TreeView clicked '''

        selection = widget.get_selection()
        model, path = selection.get_selected_rows()
        # get iter in Viewer (found entries)
        iter_v = model.get_iter(path[0])
        file_path = model.get_value(iter_v, 0) 

        file_path = file_path.replace("hiprus", "hiplib") # открываем файл из библиотеки с рус. шрифтом
        file_path = file_path.replace(".rip", ".hip")

        self.found_raw = model.get_value(iter_v, 1)    
        try:
            if face.mode:
                fp = codecs.open(file_path, "rb", "utf8")
            else:
                fp = codecs.open(file_path, "rb", "cp1251")
            f_lines = fp.readlines()
            fp.close()
        except IOError:
            print('no such file found')

        txt_ins = ''.join(f_lines)

        # create window to output selected text
        txt_win = SeText()
        txt_win.mode = face.mode

        txt_win.path1 = file_path
        txt_win.window3.set_title(face.path2name(file_path))

        if face.mode:
            txt_win.ins_txt_gr(txt_ins)
        else:
            txt_win.ins_txt_hip(txt_ins)


    def on_key_press_event(self, widget, event):
        """Callback for find-Entry in results window"""

        keyname = Gtk.keyval_name(event.keyval)

        global res_num
        selection = face.new_win.selection
        model, path = selection.get_selected_rows()
        # get iter in Viewer (found entries)
        iter_v = model.get_iter(path[0])
        file_path = model.get_value(iter_v, 0) 

        self.found_raw = model.get_value(iter_v, 1)    

        # next result is search (Viewer)
        if event.state & Gtk.Gdk.CONTROL_MASK:
            if keyname == "n" or keyname == "Cyrillic_te":
                
                if res_num < len(search_res) - 1:
                    res_num += 1
                    face.new_win.selection.select_path(search_res[res_num])
                    face.new_win.tv.scroll_to_cell(search_res[res_num])
                    face.new_win.tv.set_cursor_on_cell(search_res[res_num])

            # prevuous result is search (Viewer)
            if keyname == "p" or keyname == "Cyrillic_ze":
                if res_num != 0:
                    res_num -= 1
                    face.new_win.selection.select_path(search_res[res_num])
                    face.new_win.tv.scroll_to_cell(search_res[res_num])
                    face.new_win.tv.set_cursor_on_cell(search_res[res_num])

class Book_lst:
    '''Programm main window'''

    def __init__(self, mode=True):
    # Found entries page. 
        
        # if True - switched to greek, else - to slavonic
        self.mode = mode


        self.dash = re.compile(r"\\(с|д|р|о|г|з|к|л|т|х|ч|ъ)", re.U)
        self.punct_gr = re.compile(r"(,|\.|:|;|!|\?|·)", re.U)
        self.end_par = re.compile(r"(?P<e>^e:|^:e)(?P<rx>.*?)(?:-(?P<fl>[mis]*))?$", re.U)
        #self.end_par = re.compile(r"e:", re.U)

        # create configurations object
#        self.config = hip_config.Service('.hiptools.config')
        self.config = configparser.ConfigParser()
        self.config.read(os.path.join(os.path.expanduser('~'), '.config', 'hiptools', 'hiptoolsrc'))

        if self.mode:
#            self.lib_path = os.path.join(self.config.gr_path, 'greek_lib')
            self.lib_path = self.config.get('LibraryPaths', 'gr_path')
            self.enc = 'utf-8'
        else:
#            self.lib_path = os.path.join(self.config.sl_path, 'hiplib')
            self.lib_path = self.config.get('LibraryPaths', 'sl_path')
            self.sr_path = self.config.get('LibraryPaths', 'sl_path_ru') # путь к библиотеке славянских текстов русским шрифтом
            self.enc = 'cp1251'

         #breakpoint()
#        import pdb; pdb.set_trace()

        # delete special tags at the beginning of HIP file
        self.del_header = 1
        self.exp_lines = []
        window2 = Gtk.Window()
        window2.set_resizable(True)
        window2.set_border_width(10)
        window2.set_size_request(500, 700)

        window2.set_title("Список книг")
        window2.set_border_width(0)
        window2.connect("destroy", destroy_cb) 
        window2.connect("key_press_event", self.focus_elements) 

        box1 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=0)
        window2.add(box1)
#        box1.show()
        box2 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        box1.pack_start(box2, True, True, 0)
        box2.set_border_width(10)
#        box2.show()

        sw = Gtk.ScrolledWindow()
        #sw.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        sw.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)  # ALWAYS, NEVER.
        self.model = Gtk.TreeStore(str, str)
        self.tv = Gtk.TreeView(model=self.model)
        self.selection = self.tv.get_selection()
        self.tv.connect('row-activated', self.key_press)

        sw.add(self.tv)

        self.label = Gtk.Label() 
        
        cell1 = Gtk.CellRendererText()
        cell2 = Gtk.CellRendererText()
        self.column = Gtk.TreeViewColumn("Название книги", cell1, text=0)
        self.column2 = Gtk.TreeViewColumn("Code", cell2, text=1)

        self.tv.append_column(self.column)
        self.tv.append_column(self.column2)

        # hide second column 
        self.column2.set_visible(False)
        
        sw.show_all()
        self.tv.show()

#        self.vbox = gtk.VBox(False, 0)
#        hbox = gtk.HBox(False, 0)
        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=0)
        self.entry = Gtk.Entry()
        self.entry.connect("key_press_event", self.manager_keys) 

        if self.mode:
            # greek mode
        
#            self.search_group = self.config.default_search_group_gr
            self.search_group = self.config.get('SearchOptions', 'default_search_group_gr')
            self.lib_path = self.config.get('LibraryPaths', 'gr_path')
            self.beta = self.config.get('SearchOptions', 'betacode') # turn on beta-code input. If False - unicode greek

            self.gl_book_lst = booklst.listbooks(self.lib_path) # , False) # - чтобы не перечислять "лишние" (не описанные) файлы

            self.combo_lst = ['All_services', 'All_read', 'Minologion_base', 'Minologion extra', 'Minologion all', 'Euhologion', 'Hieratikon', 'Octoechos', 'Orologion', 'Penticostartion', 'Triodion', 'Agia_grafh', 'Psalthrion', 'Zlat_Mat', 'Zlat_John']

            min_b = []
            min_e = []
            oct_l = []
            all_l = []

            for n in ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"]:
                min_b.append("Minologion/min_base/" + n)
                if n != "apr":
                    min_e.append("Minologion/min_ext/" + n)

            for m in range(8):
                oct_l.append("Octoechos/ihos_" + str(m + 1))

            for z in [min_b, min_e, oct_l]:
                all_l.extend(z)
#        all_l.extend(['Orologion', 'Penticostarion', 'Triodion', 'Agia_grafh/Nea_diaqhkh'])
            all_l.extend(['Orologion', 'Penticostarion', 'Triodion', 'Psalthrion', 'Hieraticon', 'Euchologion'])
            all_r = ['Agia_grafh/Nea_diaqhkh', 'Psalthrion', 'st_fathers/chrisostom/Matthew', 'st_fathers/chrisostom/John']

            self.dicti = {'All_services': all_l, 'All_read': all_r, 'Minologion_base': min_b, 'Minologion extra': min_e, 'Minologion all': min_b + min_e, 'Hieratikon': ['Hieraticon'], 'Euhologion': ['Euchologion'], 'Octoechos': oct_l, 'Orologion': ['Orologion'], 'Penticostarion': ['Penticostarion'], 'Triodion': ['Triodion'], 'Agia_grafh': ['Agia_grafh/Nea_diaqhkh'], 'Psalthrion': ['Psalthrion'], 'Zlat_Mat': ['st_fathers/chrisostom/Matthew'], 'Zlat_John': ['st_fathers/chrisostom/John']}

        else:
            # slavic mode

#            self.search_group = self.config.default_search_group
#            self.lib_path = os.path.join(self.config.sl_path, 'hiplib')

            self.search_group = self.config.get('SearchOptions', 'default_search_group')
            self.lib_path = self.config.get('LibraryPaths', 'sl_path')

            self.gl_book_lst = booklst.listbooks(self.lib_path) # , False) # - чтобы не перечислять "лишние" (не описанные) файлы

            self.combo_lst = ['Богослужебные', 'Свящ. Писание', 'Все книги', 'Четьи книги', 'Акафисты', 'Алфавит', 'Апостол', 'Часослов', 'Треодь цветная', 'Добротолюбие', 'Экзапостилларии', 'Евангелия', 'Ирмологий', 'Ифика', 'Каноны', 'Молитвы на молебнах', 'Общая минея', 'Октоих', 'Пролог', 'Книга правил', 'Псалтырь', 'Треодь постная', 'Служебник', 'Типикон', 'Требник', 'Ветхий Завет', 'Минеи месячные']

            self.dicti = {'Богослужебные': ['akf','chs','ctr','kan','mol','obs','okt/glas1','okt/glas2','okt/glas3','okt/glas4','okt/glas5','okt/glas6','okt/glas7','okt/glas8','okt/ext','psa','ptr','slu','tip','trb','min/aug','min/apr','min/dec','min/feb','min/jan','min/jul','min/jun','min/mar','min/may','min/nov','min/okt','min/sep'], 
        'Свящ. Писание': ['eua', 'aps', 'vzv'], 
        'Все книги': ['akf', 'alf', 'aps', 'chs', 'ctr', 'dbr', 'eua', 'ifi', 'kan', 'mol', 'obs', 'okt', 'prl', 'prv', 'psa', 'ptr', 'slu', 'tip', 'trb', 'vzv', 'min/aug','min/apr','min/dec','min/feb','min/jan','min/jul','min/jun','min/mar','min/may','min/nov','min/okt','min/sep'],
        'Четьи книги': ['alf', 'aps', 'dbr', 'eua', 'ifi', 'prl', 'prv', 'vzv'], 'Акафисты': ['akf'], 'Алфавит': ['alf'], 'Апостол': ['aps'], 'Часослов': ['chs'], 'Триодь цветная': ['ctr'], 'Добротолюбие': ['dbr'], 'Евангелия': ['eua'], 'Ирмологий': ['irm'], 'Ифика': ['ifi'], 'Каноны': ['kan'], 'Молитвы на молебнах': ['mol'], 'Общая минея': ['obs'], 'Октоих': ['okt/glas1','okt/glas2','okt/glas3','okt/glas4','okt/glas5','okt/glas6','okt/glas7','okt/glas8','okt/ext'], 'Пролог': ['prl'], 'Книга правил': ['prv'], 'Псалтырь': ['psa'], 'Треодь постная': ['ptr'], 'Служебник': ['slu'], 'Типикон': ['tip'], 'Требник': ['trb'], 'Ветхий Завет': ['vzv'], 'Минеи месячные': ['min/aug','min/apr','min/dec','min/feb','min/jan','min/jul','min/jun','min/mar','min/may','min/nov','min/okt','min/sep']}

        self.combo = Gtk.ComboBoxText()
        #combo = Gtk.combo_box_new_text()
        self.toggle1 = Gtk.CheckButton.new_with_label("Вкл. ударения")
        self.toggle2 = Gtk.CheckButton.new_with_label("Учит. регистр")
        self.button = Gtk.Button.new_with_label('Search')
        self.pr_bar = Gtk.ProgressBar()
#        self.pr_bar.set_size_request(100,15)


        for i in self.combo_lst: 
            self.combo.append_text(i.strip())

        self.entry.show()
        self.combo.show()
        self.toggle1.show()
        self.toggle2.show()
        self.button.show()
        box1.show()
        box2.show()
        self.pr_bar.show()
        window2.show()

        box2.pack_start(self.entry, False, False, 0)
        box2.pack_start(self.combo, False, False, 0)
#        self.vbox.pack_start(self.button, True, False, 10)
        hbox.pack_start(self.toggle1, False, False, 10)
        hbox.pack_start(self.toggle2, False, False, 10)
        hbox.pack_end(self.button, False, False, 10)
        box2.pack_start(hbox, False, False, 0) #1й True все портит
        box2.pack_start(self.pr_bar, False, False, 0)
#        for i in self.combo_lst: 
#            self.combo.append_text(i.strip())

#        self.entry.show()
#        self.combo.show()
#        toggle1.show()
#        toggle2.show()
#        button.show()
#        hbox.show()
#        pr_bar.show()
#        window.show()

        box2.pack_start(self.label, False, False, 0)
        box2.pack_start(sw, True, True, 0)

#        self.label.show()

#        window2.show()

        self.book_lst = booklst.listbooks(self.lib_path) # , False) # - чтобы не перечислять "лишние" (не описанные) файлы

        self.split_parag = re.compile(u'(?:\r?\n){2,}', re.U)
        self.kill_rn = re.compile(u'(?:\r?\n)', re.U)
        self.html_del = re.compile(r'<.*?>', re.S)



        # passes two objects: itself and the widget:
        self.combo.connect("changed", self.choose)       
        for x in range(len(self.combo_lst)):
            if self.search_group == self.combo_lst[x]:
                self.combo.set_active(x)

        # define checkboxes callbacks
        self.toggle1.connect("toggled", self.stress)
        self.toggle2.connect("toggled", self.case_switch)       

        self.case_ins = True

        # check config for parameter
        if self.config.get('SearchOptions', 'diacritics_off') == 'True':
            self.stress_toggle = False
        elif self.config.get('SearchOptions', 'diacritics_off') == 'False':
            self.stress_toggle = True
        if self.config.get('SearchOptions', 'punct_del') == 'True': #delete punctuation in
            # searched line
            self.punct_del = True
        elif self.config.get('SearchOptions', 'punct_del') == 'False':
            self.punct_del = False

        self.comm_lst = []
        self.pointer = 0

        self.button.connect('clicked', self.search_it)

        def putdir(obj, parent=None):
            """
            Рекурсивная функция дла добавления элементов в дерево
            """
            for book in obj:
                iter = self.model.append(parent)
                self.model.set(iter, 0, book[1])
                self.model.set(iter, 1, book[0])
                if book[2]: # subdir
                    putdir(book[2], iter)
        putdir(self.book_lst)

    def focus_elements(self, widget, event):
        # фокусируемся на поиске, индексе книжек, или выборе книжек для поиска
        #if event.state & Gtk.Gdk.CONTROL_MASK:
        if event.get_state() & Gdk.ModifierType.CONTROL_MASK:
            keyname = Gdk.keyval_name(event.keyval)
            if keyname == "f" or keyname == "Cyrillic_ve":
                self.entry.grab_focus()
            elif keyname == "l" or keyname == "Cyrillic_ze":
                self.tv.grab_focus()
            elif keyname == "b" or keyname == "Cyrillic_i":
                self.combo.grab_focus()
            elif keyname == "p" or keyname == "Cyrillic_ze":
#                gui = gui_conf.GuiOpt()
                opt = gui_conf.Options(gui_conf.GuiOpt())
            elif keyname == "h" or keyname == "Cyrillic_er":
                text = ["Настройки Ctrl+p",
                        "Помощь Ctrl+h",
                        "Выбрать поиск Ctrl+f",
                        "Выбрать список Ctrl+l",
                        "Выбрать книгу/группу поиска Ctrl+b"]
                inf = InfoPop("\n\n".join(text))

    def count_files(self, books):

        file_count = 0

        for book in books:
#        book = ''.join(["/usr/local/lib/hip-tools/hiplib/", book])

#            book = os.path.join(os.getcwd(), "hiplib", book)
            book = os.path.join(self.lib_path,  book)

            for file in os.listdir(book):
                if file.endswith('hip') or file.endswith('xml')  or file.endswith('rip'):
                    file_count += 1

#        print(file_count)

        return file_count


    def f_search(self, books=None, word=None, reg_true=False):
        '''main search function'''

        global count # can't move, won't work
        print('word', word)

        # number of 'hip' files in given group of books
        num_files = self.count_files(books)
        cur_file = 0

        res = []
        count = 0

        if self.mode:
            word = unicodedata.normalize('NFD', word)
        print("searching for ", word)
        if reg_true:
            patt = word
            if not self.mode: # в славянском надо заэкранировать титла
                if self.dash.search(word): # ищем титла
                    patt = self.dash.sub(r"\\\1", word) # \b, \s оставляем c одним слэшем, остальное набиваем, чтобы искались титла. 

#TODO: Проверить как работает re.escape ^^^

        elif word.startswith('\"'):
            patt = word.replace('\"', '\\b')
#            patt = "r'\s" + word + "\s'"

        elif word.endswith('*'):
            word = word.replace('*', '')
#        patt = word + ".*\s"
            #patt = "\s" + word + ".*?\s"
            patt = "\\b" + word + ".*?\\b"
#        print(patt)

        elif word.startswith('*'):
            word = word.replace('*', '')
#            patt = "\s\w+?" + word + "\s"
            patt = "\\b\\w+?" + word + "\\b"
#        word = '\s\w+?гул[\s\n\.,;:!\?@"\']'
#        word = '\s\w+?[\s\n\.,;:!\?@"\']'
#        print(patt)

        else:
            patt = word

        if face.case_ins:
            regex = re.compile(patt, re.U | re.I)
        else:
            regex = re.compile(patt, re.U)

        for j in books:
#            j = os.path.join(self.lib_path, j)
            if self.mode:
                j = os.path.join(self.lib_path, j)
            else:
                j = os.path.join(self.sr_path, j)
            print(j)

            f_names = os.listdir(j)
#            print(f_names)

            for i in f_names:
#                if i.endswith('.hip') or i.endswith('.xml'):
                if i.endswith('.hip') or i.endswith('.xml') or i.endswith('.rip'):
                    cur_file += 1
#                    perc = round(float(cur_file)/float(num_files), 1)

                    # calculate percent of work done so far
                    perc = float(cur_file)/float(num_files)
                    #TODO: сделать полоску толстой (где-то True в pack поставить)
                    face.pr_bar.set_fraction(perc)
                    # these are nesessary to steal control from Gtk main loop
                    # else progrees bar wont move untill job's done
                    while Gtk.events_pending():
                        Gtk.main_iteration()

                    file_path = os.path.join(j, i)
#                    print("file_path: ", file_path)
                    if self.mode:
                        fp = codecs.open(file_path, "rb", "utf-8")
                    else:
                        #fp = codecs.open(file_path, "rb", "cp1251")
                        fp = codecs.open(file_path, "rb", "utf-8")


                    f_lines = fp.readlines()
                    fp.close()

                    for str_num in range(len(f_lines)):
                        line = f_lines[str_num]

                        if self.punct_del:
                            line = self.punct_gr.sub('', line)
                            # убрать [.,:;?!...] из искомой строки. +греческая средняя точка

                        # clean all apostrophs (stresses) from searched text
                        if self.mode:
                            for a in [u'\u0300', u'\u0342', u'\u0301', u'\u0314', u'\u0313']:
                                line = line.replace(a, '')
                        else:

                            if not face.stress_toggle:
#                                line = russ.conv_str(line) # ищем в русском представлении, без титл... Ооочень долго
                                line = line.replace('\'', '')
#                                line = line.replace('`', '') # уже есть в russ_simp
#                                line = line.replace('^', '')
                                line = line.replace('=', '') # а то найдет о=коло при запр. "коло". Теперь не надо в поиске набирать = (придыхание)
                            if '%<' or '%>' in line: # TODO: переделать регекспом. Не ловит %<%>
                                line = line.replace('%<','')
                                line = line.replace('%>', '')

                        if regex.search(line):
                            count += 1
                            # convert hip to russian
                            rus_txt = russ.conv_str(f_lines[str_num])
                            res.append((file_path, str_num, rus_txt))
        #breakpoint()
            
        return res

    def manager_keys(self, widget, event):
        '''Hipsearch bindings for main window. key_press is for hipindex'''

#        if event.state & Gtk.Gdk.CONTROL_MASK:
#        keyname = Gtk.Gdk.keyval_name(event.keyval)
        keyname = Gdk.keyval_name(event.keyval)

        if keyname == "Return":
#            print('Return')
            self.search_it()

        if keyname == "Up":
            print('Up')
#            if self.comm_lst:

            self.pointer += 1
            print('pointer', self.pointer)
            if self.pointer <= len(self.comm_lst):
                self.entry.set_text(self.comm_lst[len(self.comm_lst) - self.pointer])
            else:
                self.pointer == 1
                self.entry.set_text(self.comm_lst[len(self.comm_lst) - self.pointer])
 
        if keyname == "Down":
            print('Down')

    def choose(self, widget):
        '''Chooses the books group'''

        get_name = widget.get_active_text() 

        for name in self.dicti:
            if get_name == name:
                self.books = self.dicti[name]

    def case_switch(self, widget, data=None):

       if widget.get_active():
           self.case_ins = False
       else:
           self.case_ins = True 
        
    def stress(self, widget, data=None):

       if widget.get_active():
           self.stress_toggle = True 
       else:
           self.stress_toggle = False
         
    def search_it(self, button=None):
        '''Main search interface'''

        reg_true = False # Свитч, есть ли "e:" в запросе, нужно ли распознать рег. выражение.

        self.s_word = self.entry.get_text()
#        print('s_word', self.s_word)

        ch_reg = self.end_par.search(self.s_word)
        if ch_reg:
            self.s_word = ch_reg.group('rx')
            # средняя часть строки вида "e:regex-i", именованная группа
            # теперь флаги должны работать без "e:" в начале (т.е. и без регекспов)
            if ch_reg.group('e'):
                reg_true = True
            if ch_reg.group('fl'):
                if "r" in ch_reg.group('fl'): # -r (case-sensitive), инвертированное -i.
                    self.case_ins = False
                if "p" in ch_reg.group('fl'): # -p оставить пунктуацию в строке поиска
                    self.punct_del = False
       
        if self.mode and self.beta:
            self.s_word = bcode.convert_all(self.s_word)

        self.comm_lst.append(self.s_word)
#        print(self.comm_lst)

        # res - list of tuples, containing path, line_num, text
        res = self.f_search(self.books, self.s_word, reg_true)
        if res:
            self.new_win = Viewer() # window with the list of found results
            self.new_win.ins(res)
            self.new_win.selection.select_path((0,))
            self.new_win.tv.set_cursor((0,))

        else:
            print('Didn\'t find anything, sorry.')
            Popup("Ничего не найдено")

    def path2name(self, f_path):
        ''' Convert given path to description string '''

        self.path_ls = []

        while(f_path):
            f_path, tail = os.path.split(f_path)
            if tail == 'hiplib' or tail == 'greeklib':
                break
            self.path_ls.insert(0, tail)
                    
        self.desc_str = []
        self.walker(self.gl_book_lst)
        res_str = ' / '.join(self.desc_str)

        return res_str

    def walker(self, book_lst):
        '''recursive funstion for parsing through book list'''

        for book in book_lst:
            if self.path_ls:
                if self.path_ls[0] in book[0]:
                    out = self.path_ls.pop(0)
                    self.desc_str.append(book[1])
                    if book[2]:
                        self.walker(book[2])
            else:
                break


    def key_press(self, tv, path, column):
        """Callback for hipindex Viewer. manager_keys is for hipsearch"""

        # expand or collapse rows
        if not path in self.exp_lines: 
            tv.expand_row(path, False)
            self.exp_lines.append(path)
        else:
            tv.collapse_row(path)
            self.exp_lines.remove(path)

        iter_cur = self.model.get_iter(path)

        if not self.model.iter_has_child(iter_cur):
            dir_name_x = ""
            dir_out = []
            iter_par_c = iter_cur
            f_name = self.model.get_value(iter_cur, 1)

#            print(self.model.get_value(iter_par_c, 1)

            while True:
                iter_par_p = self.model.iter_parent(iter_par_c)
                dir_name_p = self.model.get_value(iter_par_p, 1)
                dir_out.append(dir_name_p)
                iter_par_c = iter_par_p
                if not self.model.iter_parent(iter_par_p):
                    break

            dir_out.reverse()
            f_path_list = [self.lib_path, '/']
            f_path_list.extend(dir_out)
            f_path_list.append(f_name)
            f_path = ''.join(f_path_list)

            print('run', f_path)
            fp = codecs.open(f_path, "rb", self.enc)

            f_lines = fp.readlines()
            fp.close()

            if self.mode:
                xmldoc = minidom.parseString(f_lines[1] + '</document>')
                xml_nodes = xmldoc.getElementsByTagName('document')
                for item in xml_nodes:
                    t_name = item.getAttribute('title') 
                    break

            else:
            # aweful crutch: delete service tags in the beginning of the file
            # DO Something!
                if self.del_header:
                    for z in range(10):
                        if "<::" in f_lines[z]:
                            f_lines.pop(f_lines.index(f_lines[z]))
                t_name = self.model.get_value(iter_cur, 0)


            # create window to output selected text
            txt_win = Show_text(self.mode)
            
            txt_win.path1 = f_path

            iter_par = self.model.iter_parent(iter_cur)
            title = ' / '.join([self.model.get_value(iter_par, 0), t_name])

            txt_win.window3.set_title(title)

            # insert text
            text_ls = []
            txt = ''.join(f_lines)

#            parts_ls = self.split_parag.split(txt)
#            for part in parts_ls:
#                part = self.kill_rn.sub(' ', part)
#                text_ls.append(part)
#            txt1 = '\n\n'.join(text_ls)

######            import pdb; pdb.set_trace()
            if self.mode:
                txt = self.html_del.sub('', txt)
                txt_win.ins_txt_gr(txt)
            else:
                txt_win.ins_txt_hip(txt)


#            txt_win.style_txt()
class InfoPop:
    def __init__(self, text=None):

        self.text = text
        self.win = Gtk.Window()
        #self.win.set_type_hint(Gdk.WindowTypeHint.UTILITY)
        #self.win.set_type_hint(Gdk.WindowTypeHint.DIALOG)
        self.win.set_size_request(650, 400)
        self.win.set_modal(False)

        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        vbox.set_homogeneous(True)

        self.win.add(vbox)

        label_1 = Gtk.Label(label=text)
        label_1.set_line_wrap(True)
        #label_1.set_selectable(True)
        label_1.set_max_width_chars(32)
        label_1.set_justify(Gtk.Justification.FILL)

        vbox.pack_start(label_1, True, True, 10)

        self.win.show_all()
 
def main():
    Gtk.main()
    return 0

if __name__ == '__main__':

    from optparse import OptionParser
    usage = "usage: %prog [options] dir"
    parser = OptionParser(usage=usage)

    parser.add_option("-g", "--greek", dest="greek", action="store_true", help="Switch to greek")
    parser.add_option("-s", "--slav", dest="slav", action="store_true", help="Switch to slavonic")
    
    (options, args) = parser.parse_args()
#    if args:
    if options.greek:
        face = Book_lst(True)
    elif options.slav:
        face = Book_lst(False)
    else:
        print('no parameters given, exiting')
        sys.exit(0)


    main()

#TODO: исчезающее меню (его и так сейчас не видно. box2?)
#TODO: попробовать сделать управление hjkl в окне выбора файла

# кнопу для принудительного обновления базы
# объединить славянский и греческий блок в одну горизонтальную коробку
# TODO: поиск по Book_lst (найти книги, разделы, напр "Требник", "Чин Крещения"... Найденный пункт TreeView должен разворачиваться
# TODO: переделать слав. поиск, без титл и проч.
# TODO: попап с хелпом по хоткеям.
