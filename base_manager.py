#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import codecs
import sqlite3
import re
import os
import sys

class Base:
    def __init__(self):
        
        self.b_name = os.path.join(os.path.expanduser('~'), 'hiptools', 'hip_base.db')
        self.white = re.compile(u'(\r|\n|\r\n)', re.M)
        self.tab_name='my_table'
        self.plus = re.compile(r'^\+ ?', re.U)
    def parser_x(self, line):
        '''split line into pieces for insertion into base'''        

        if line.startswith('#'):
            return None

        # clean new lines
        line = self.white.sub('', line)

        if '||' in line:
            data = line.split('||')
        elif '//' in line:
            data = line.split('//')
        else:
            print('no delimiter in line', n)
            return None

        return data

 
    def get_f_base(self, command):
        '''technical utility for pr_base()'''

        conn = sqlite3.connect(self.b_name)
        cur = conn.cursor()

        cur.execute(command)
        res = cur.fetchall()

        cur.close()

        return res

    def get_to_base(self, command):
        '''technical utility for pr_base()'''

        conn = sqlite3.connect(self.b_name)
        cur = conn.cursor()
        cur.execute(command)
        conn.commit()
        cur.close()

    def create_base(self):

        conn = sqlite3.connect(self.b_name)
        cur = conn.cursor()

        # base: uuid, date, self.etype, gpath1, gpath2, gpath3, gpath4, gtext1, gtext2 
        command = "CREATE TABLE " + self.tab_name + " (uuid TEXT, date TEXT, etype TEXT, gpath1 TEXT, gpath2 TEXT, text_att1 TEXT, text_att2 TEXT, gtext1 TEXT, gtext2 TEXT)"
        cur.execute(command)
        #print('successfully created base')
        #print('no tab name given, exit')

        cur.close()

    def wr_base(self, wr=False, d_name='base_dump.txt'):
        """Dump base to a text file"""
        #breakpoint()
        command = "SELECT * FROM " + self.tab_name + " ORDER BY date"
        rows = self.get_f_base(command)

        out_l = []

        for row in rows:
            itms = []

            for itm in [row[0], '||', row[1], '||', row[2], '||', row[3], '||', row[4], '||', row[5], '||', row[6], '||', row[7], '||', row[8]]:
                n_itm = itm.replace('\n', '; ')
                n_itm = n_itm.strip()
                if not n_itm:
                    n_itm = "None"
                itms.append(n_itm)

            out_l.append(''.join(itms))
            out_l.append('\n')
        if wr:
            utf = codecs.open(d_name, "wb", "utf-8")
            for i in out_l:
                utf.write(i)
                print('writing ', d_name)
            utf.close()
        else:
            for i in out_l:
                print(i)

    def parse_txt(self, f_lines):
        """Parse data from file"""

        for n in range(len(f_lines)):
#            data = self.reg.findall(line)[0]
            line = f_lines[n]
            line = line.strip()
            # commentaries (e.g. text already inserted into base)
            data = self.parser_x(line)           

            if len(data) > 9:
                print('too many chunks in', n)
            if len(data) < 9:
                print('too few chunks in', n)
#            for i in data:
#                print i.strip()
            else:
                self.ins_base(data)
#                print 'OK in', n

    def ins_base(self, data):
        # data = (word_greek, word_slav, description)
        
        conn = sqlite3.connect(self.b_name)
        cur = conn.cursor()
        cur.execute("INSERT INTO " + self.tab_name + " (uuid, date, etype, gpath1, gpath2, text_att1, text_att2, gtext1, gtext2) VALUES (?,?,?,?,?,?,?,?,?);", data)
        conn.commit()
        cur.close()

        print('Inserted: ' + ', '.join(data[1:]) + ' into base')
 
    def update_from_file(self, data):
        '''Update entry in the table'''
        # idea: проверь ячейки той таблицы, которые помечены +. 
        # select * from ... where uuid=..., обрабатываем parser_x(), сравниваем списки 
        for ln in data:
            if ln.startswith("+"):
                ln = self.plus.sub("", ln)  # удалим плюс в начале, можно с пробелом
                line_ls = self.parser_x(ln)
                for i in range(len(line_ls)):
                    if line_ls[i] == "None":
                        line_ls[i] = ""
                command = "SELECT * FROM " + self.tab_name + " WHERE uuid=\"" + line_ls[0] + "\""
                res = self.get_f_base(command)
#                print('res', res[0])
#                print('ls', line_ls)
                diff_l = []
                for y in range(len(res[0])):
                    if res[0][y] != line_ls[y]:
                        diff_l.append([y, line_ls[y]])

                att_ls = ["uuiu=", "date=", "etype=", "gpath1", "gpath2", "text_att1", "text_att2", "gtext1", "gtext2"]

                command = "UPDATE " + self.tab_name + " SET "
                if diff_l:
                    j = 1
                    for x, st in diff_l:
                        print('att', x, st, att_ls[x])
                        command += att_ls[x] + "=\"" + st + "\""
                        if j < len(diff_l):
                            command += ", "
                        j += 1
                    command += " WHERE uuid=\"" + line_ls[0] + "\""
                    print(command)
                    self.get_to_base(command)


#                command = "UPDATE " + self.tab_name + " SET " + "gtext1=\"" + line_ls[7] + "\" WHERE uuid=\"" + line_ls[0] + "\""
#                print('command', command)
#
#                self.get_to_base(command)

#                tofix.append(line_ls)

#        print(tofix)



if __name__ == '__main__':

    from optparse import OptionParser
    usage = "usage: %prog [options] word1 [word2] description"
    parser = OptionParser(usage=usage)

    parser.add_option("-c", "--create", dest="create", action="store_true", help="Create new base")
    parser.add_option("-p", "--print_b", dest="print_b", action="store_true", help="Print base entries as formatted text into file")
#    parser.add_option("-o", "--output", dest="output", action="store", help="Filename to dump base to")
    parser.add_option("-r", "--read", dest="read", action="store_true", help="Read text from file, parse and insert into base")
    parser.add_option("-l", "--line", dest="line", action="store_true", help="Read text from string, parse and insert into base")
    parser.add_option("-s", "--search", dest="search", action="store_true", help="Read text from string, parse and insert into base")
    parser.add_option("-u", "--update", dest="update", action="store", help="Update text from file")
    parser.add_option("-w", "--write", dest="write", action="store", help="Dubp data to a file")
    parser.add_option("-t", "--table", dest="table", action="store_true", help="make table from data")

    (options, args) = parser.parse_args()

    #if args:
    if options.create:
        main_f = Base()
        main_f.create_base()
    elif options.write:
        # print base entries into text file
        main_f = Base()
        main_f.wr_base(True, options.write) #  куда писать
    elif options.print_b:
        main_f = Base()
        main_f.wr_base(False) 

    elif options.read:
        # read file,
        # isert formatted lines into base 
        # (separator: // or ||)
        # args: 0=base, 1=tab, 2=file_name

        main_f = Base()
        fp = codecs.open(args[0], "rb", "utf-8") # имя файла для читки
        f_lines = fp.readlines()
        fp.close()

        # strings and table name to parse 
        main_f.parse_txt(f_lines)  # назв таблицы

#    elif options.line:
#        # isert formatted line into base (separator: // or ||)
#        # args: 0=base, 1=tab, 2=string
#        main_f = Base()
#        string1 = args[2].decode('utf8')
#        # str and table name to parse 
#        main_f.parse_txt([string1,], args[1])
#
#    elif options.search:
#        # args: 0=dict, 1=tab, 2=word, 3=col
#        main_f = Base()
#        string1 = args[2].decode('utf8')
#        if len(args) < 4:
#            col = "0"
#        else:
#            col = args[3]
#        # search word, dict, column
#        main_f.search_txt(string1, args[1], col)
#    
    elif options.update:
        # update entry in the table (not tested)
        main_f = Base()
        fp = codecs.open(options.update, "rb", "utf-8") # имя файла для читки
        f_lines = fp.readlines()
        fp.close()
        
        main_f.update_from_file(f_lines)
    else:
        print('no option, don\'t know what to do, exiting')
        sys.exit(0)
#        main_f = Base()


