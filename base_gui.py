#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk

#import codecs
#import re
import os
#import sys
import base_manager
import configparser
import time
import uuid
import sqlite3

import get_par

bsm = base_manager.Base()

def destroy_cb(widget):
    Gtk.main_quit()
    return False

class Popup:
    def __init__(self, text=None):
#        self.title = ""
        self.text = text
        dialog = Gtk.Dialog(title='warning')
        label = Gtk.Label()
        label.set_text(self.text)
        dialog.vbox.pack_start(label, True, True, 10)
        label.show()
        dialog.show()

class CommentBox:
    '''Work window to write comments on greek or slavic texts (errors, difficult places...)

    '''
    def __init__(self, path1=None, prnt=None, bgui=None):

        if prnt == "base":
            self.bg = bgui

            #print("bgui", self.bg)
#        else:
#            self.bg = bg

        self.prnt = prnt

        #        self.id = str(uuid.uuid4())
        if "greeklib" in path1:
            mode = True # greek
        else:
            mode = False
        path2 = get_par.Par(mode).open_par(path1)
        if path2:
            path2 = path2.rstrip()

        self.b_name = os.path.join(os.path.expanduser('~'), "hiptools/hip_base.db")
        #TODO: get path from config

        print("original path ", path1)
        print("parallel path ", path2)
        window2 = Gtk.Window()
        # window2.set_resizable(True)
        #window2.set_border_width(10)
        window2.set_size_request(950, 450)
        window2.set_title("Создайте комментарий")
        window2.connect('key_press_event', self.buttony)

        boxy1 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=0)
        window2.add(boxy1)
        boxy1.show()
#        box2 = Gtk.VBox(False, 10)
        boxy2 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        boxy2.set_border_width(10)
        boxy1.pack_start(boxy2, True, True, 0)
        boxy2.show()
        boxy3 = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=0)
        boxy3.show()
        boxy4 = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=0)
        boxy4.show()

        boxy5 = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=0)
        boxy5.show()

        self.combo1 = Gtk.ComboBoxText()  # choose type of error
        self.entry1 = Gtk.Entry()
        self.entry2 = Gtk.Entry()
        self.entry3 = Gtk.Entry()
        self.entry4 = Gtk.Entry()

        if path1:
            self.entry1.set_text(path1)
        if path2:
            self.entry3.set_text(path2)

        self.combo1.show()
        self.entry1.show()
        self.entry2.show()
        self.entry3.show()
        self.entry4.show()

        sw1 = Gtk.ScrolledWindow()
        sw2 = Gtk.ScrolledWindow()
        sw1.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)  # ALWAYS, NEVER.
        sw2.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)  # ALWAYS, NEVER.
#        sw1.connect('key_press_event', self.buttony) # может, лучше к окну прицепить?
#        sw2.connect('key_press_event', self.buttony) # может, лучше к окну прицепить?
        self.textview1 = Gtk.TextView()
        self.textview2 = Gtk.TextView()
        self.textview1.set_editable(True)
        self.textview2.set_editable(True)
        self.textview1.show()
        self.textview2.show()

        self.textbuffer1 = self.textview1.get_buffer()
        self.textbuffer2 = self.textview2.get_buffer()
#        self.iter = Gtk.TextIter(self.textbuffer)
        sw1.add(self.textview1)
        sw1.show()
        sw2.add(self.textview2)
        sw2.show()

#        sw1.connect('key_press_event', self.redraw_cb)
        boxy3.pack_start(self.entry1, True, True, 0)
        boxy3.pack_start(self.entry2, True, True, 0)
        boxy4.pack_start(self.entry3, True, True, 0)
        boxy4.pack_start(self.entry4, True, True, 0)
        boxy2.pack_start(self.combo1, False, False, 0)
        boxy2.pack_start(boxy3, False, False, 0)
        boxy2.pack_start(boxy4, False, False, 0)
        boxy5.pack_start(sw1, True, True, 2)
        boxy5.pack_start(sw2, True, True, 2)
        boxy2.pack_start(boxy5, True, True, 0)
        window2.show() 

        for i in ['Ошибка сл. перевода', 'Различие в рукописях', 'Неясное выражение в греческом']:
            self.combo1.append_text(i.strip())

        self.combo1.set_active(0)
        self.etype = 'tr_err'

        self.combo1.connect("changed", self.choose_me) 

        self.err_types = {'Ошибка сл. перевода': 'tr_err', 'Различие в рукописях': 'manus_diff', 'Неясное выражение в греческом': 'gr_uncl'}

    def choose_me(self, widget):

        get_name = widget.get_active_text() 

        for name in self.err_types:
            if get_name == name:
                self.etype = self.err_types[name] # тип ошибки

    def buttony(self, widget, event):
        '''get stuff from form'''

        keyname = Gdk.keyval_name(event.keyval)
        if (keyname == "s" or keyname == "Cyrillic_yeru") and event.get_state() & Gdk.ModifierType.CONTROL_MASK:
            gpath1 = self.entry1.get_text()
            gpath2 = self.entry2.get_text()
            gpath3 = self.entry3.get_text()
            gpath4 = self.entry4.get_text()

            startiter, enditer = self.textbuffer1.get_bounds()
            gtext1 = self.textbuffer1.get_text(startiter, enditer, True) # в конце --- include hidden attribute, new in gtk3
            startiter, enditer = self.textbuffer2.get_bounds()
            gtext2 = self.textbuffer2.get_text(startiter, enditer, True) 

            yd = time.strftime('%Y/%m/%d %H:%M', time.localtime())
            #yd = str(d[0]) + "/" + str(d[1]) + "/" + str(d[2]) + " " + str(d[3]) + ":" + str(d[4])

            data = (str(uuid.uuid4()), yd, self.etype, gpath1, gpath3, gpath2, gpath4, gtext1, gtext2)

            #return (gpath1, gpath2, gpath3, gpath4, gtext)
            ins = self.ins_base(data, 'my_table')

            if ins:
                Popup("Записал")
            else:
                Popup("Что-то не получилось")
            
            if self.prnt == "base":

                self.bg.model.clear()

                command = "SELECT * FROM my_table ORDER BY date"
                rows = bsm.get_f_base(command)

                self.bg.insert_f_base(rows)
#                bg.selection.select_path((0,))

    def ins_base(self, data, tab_name=None):
        # data = (word_greek, word_slav, description)
        
        conn = sqlite3.connect(self.b_name)
        cur = conn.cursor()
            
#        print("INSERT INTO " + tab_name + " (uuid TEXT, date TEXT, etype TEXT, gpath1 TEXT, gpath2 TEXT, text_att1 TEXT, text_att2 TEXT, gtext TEXT) VALUES (?,?,?,?,?,?,?,?);", data)
        cur.execute("INSERT INTO " + tab_name + " (uuid, date, etype, gpath1, gpath2, text_att1, text_att2, gtext1, gtext2) VALUES (?,?,?,?,?,?,?,?,?);", data)

        conn.commit()
        cur.close()

        return True       

class Base_gui:
    '''Main prog window. Show records in a base'''
    def __init__(self):
        
        self.rows = []

        self.config = configparser.ConfigParser()
        self.config.read(os.path.join(os.path.expanduser('~'), '.config', 'hiptools', 'hiptoolsrc'))

        self.res_num = 0
#        self.text = 'Searching...'
        window2 = Gtk.Window()
        window2.set_resizable(True)
        window2.set_size_request(950, 450)

        window2.set_title("База комментариев")
        window2.set_border_width(0)
#        window2.set_border_width(10)
#        window2.connect('key_press_event', on_key_press_event)
        #window2.connect("destroy", destroy_cb) 
        window2.connect('key_press_event', self.buttony2)
        if __name__ == '__main__':
            window2.connect('destroy', destroy_cb)

#        box1 = Gtk.VBox(False, 0)
        box1 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=0)
        window2.add(box1)
        box1.show()
#        box2 = Gtk.VBox(False, 10)
        box2 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        box2.set_border_width(10)
        box1.pack_start(box2, True, True, 0)
        box2.show()

        sw = Gtk.ScrolledWindow()
        sw.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)  # ALWAYS, NEVER.
        self.model = Gtk.ListStore(str, str, str, str, str, str, str, str)
        #self.model = Gtk.ListStore(str, str, str, str, str)
        self.tv = Gtk.TreeView(model=self.model)
        self.selection = self.tv.get_selection()

        self.tv.connect('row-activated', self.on_click)

        self.modelfilter = self.model.filter_new()
#        self.modelfilter.set_visible_func(vis, self.small)
        self.tv.set_model(self.modelfilter)

        sw.add(self.tv)

        self.label = Gtk.Label() 
        self.entry = Gtk.Entry()
        
        cell1 = Gtk.CellRendererText()
        cell2 = Gtk.CellRendererText()
        cell3 = Gtk.CellRendererText()
        cell4 = Gtk.CellRendererText()
        cell5 = Gtk.CellRendererText()
        cell6 = Gtk.CellRendererText()
        cell7 = Gtk.CellRendererText()
        cell8 = Gtk.CellRendererText()

        # font to render found entries in Viewer is aquired from config
#        cell1.set_property('font', self.config.get('Fonts', 'reg_font'))
#        cell2.set_property('font', self.config.get('Fonts', 'reg_font'))

        self.column1 = Gtk.TreeViewColumn("Дата", cell1, text=0)
        self.column2 = Gtk.TreeViewColumn("Путь 1", cell2, text=1)
        self.column3 = Gtk.TreeViewColumn("Путь 2", cell3, text=2)
        self.column4 = Gtk.TreeViewColumn("Ссылка 1", cell4, text=3)
        self.column5 = Gtk.TreeViewColumn("Ссылка 2", cell5, text=4)
        self.column6 = Gtk.TreeViewColumn("Проблема", cell6, text=5)
        self.column7 = Gtk.TreeViewColumn("Решение", cell7, text=6)
        self.column8 = Gtk.TreeViewColumn("uuid", cell8, text=7)

#        col_lst = [self.column1, self.column2, self.column3, self.column4, self.column5, self.column6, self.column7]
#        for c in col_lst:
#            self.tv.append_column(c)

        self.tv.append_column(self.column1)
        self.tv.append_column(self.column2)
        self.tv.append_column(self.column3)
        self.tv.append_column(self.column4)
        self.tv.append_column(self.column5)
        self.tv.append_column(self.column6)
        self.tv.append_column(self.column7)
        self.tv.append_column(self.column8)
        
        #self.tv.set_search_column(2)
        
        sw.show_all()
        self.tv.show()
        box2.pack_start(self.label, False, False, 0)
        box2.pack_start(sw, True, True, 0)
        box2.pack_start(self.entry, False, False, 0)

#        self.entry.connect('activate', self.entry_cb)
#        self.entry.connect('key_press_event', self.on_key_press_event)
#        self.label.set_text("Найдено " + str(count) + " совпадений")
        self.entry.show()
        self.label.show()

        window2.show()

        #self.rows = self.insert_f_base()

        self.column3.set_visible(False)
        self.column5.set_visible(False)
        self.column8.set_visible(False)

    def insert_f_base(self, rows=None):
        '''fit data from the base into the ListStore'''

        p1 = ''
        p2 = ''
        for r in rows:
            if "hiptools" in r[3]:
                p1 = r[3].split("hiptools")[1]
            else:
                p1 = r[3]
            if "hiptools" in r[4]:
                p2 = r[4].split("hiptools")[1]
            else:
                p2 = r[4]
            self.model.append([r[1], p1, p2, r[5], r[6], r[7], r[8], r[0]]) # Вторую скобку в конце не забывай! ]]

        return rows

    def on_click(self, widget, iter, path):
        ''' callback, row in TreeView clicked '''

        selection = widget.get_selection()
        model, path = selection.get_selected_rows()
        # get iter in Viewer (found entries)
        iter_v = model.get_iter(path[0])
        file_path = model.get_value(iter_v, 0) 

        clicked_uuid = model.get_value(iter_v, 7) # uuid
        if not self.rows:
            command = "SELECT * FROM my_table WHERE uuid LIKE \"" + clicked_uuid + "\""

            self.rows = bsm.get_f_base(command)
        #    self.rows = self.insert_f_base(rows)
        # что экономнее -- дергать базу, или перебирать уже существующие данные?
        for i in self.rows: # данные из базы
            if i[0] == clicked_uuid: # срав uuid
                cbox = CommentBox(i[3], "base", self)
                cbox.entry2.set_text(i[5])
                cbox.entry4.set_text(i[6])
                cbox.textbuffer1.set_text(i[7])
                cbox.textbuffer2.set_text(i[8])

    def buttony2(self, widget, event):
        '''get stuff from form'''

        keyname = Gdk.keyval_name(event.keyval)

        if (keyname == "n" or keyname == "Cyrillic_te") and event.get_state() & Gdk.ModifierType.CONTROL_MASK:
            cbox = CommentBox("", "base", self) # передаем parent object, Base_gui()

        elif (keyname == "d" or keyname == "Cyrillic_ve") and event.get_state() & Gdk.ModifierType.CONTROL_MASK:
            # delete stuff from base
            selection = self.tv.get_selection()
            model, path = selection.get_selected_rows()
            # get iter in Viewer (found entries)
            iter_v = model.get_iter(path[0])
            file_path = model.get_value(iter_v, 0) 

            ud = model.get_value(iter_v, 7) # uuid
            print(ud)

            command = "DELETE FROM my_table WHERE uuid=\"" + ud + "\""

            bsm.get_to_base(command)
            Popup("Удалено")

            self.model.clear()

            command = "SELECT * FROM my_table ORDER BY date"
            rows = bsm.get_f_base(command)

            self.insert_f_base(rows)

#            print('path', path.get_indices())

            #breakpoint()
            #selection = self.tv.get_selection()
#            model, path = selection.get_selected_rows()
            #model, path = selection.get_selected()
            # get iter in Viewer (found entries)
            #iter_v = model.get_iter(path[0])
            #bg.selection.select_iter(iter_v)

#            bg.selection.select_path((0,))

        elif (keyname == "r" or keyname == "Cyrillic_ka") and event.get_state() & Gdk.ModifierType.CONTROL_MASK:
            self.model.clear()
            command = "SELECT * FROM my_table ORDER BY date"
            rows = bsm.get_f_base(command)
            self.insert_f_base(rows)
#            self.selection.select_path((0,))  # нужен объект TreePath

def main():
    Gtk.main()
    return 0

if __name__ == '__main__':

    from optparse import OptionParser
    usage = "usage: %prog [options] dir"
    parser = OptionParser(usage=usage)

    parser.add_option("-g", "--greek", dest="greek", action="store_true", help="Switch to greek")

    (options, args) = parser.parse_args()

    bg = Base_gui()

    command = "SELECT * FROM my_table ORDER BY date"
    rows = bsm.get_f_base(command)

    bg.rows = bg.insert_f_base(rows)

    main()
#TODO: add delete function to remove wrong entries  to main window (-string in cli)
