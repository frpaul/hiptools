#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''Читает и отображает данные конфигурации программы hts.py для работы с церковно-славянскими и греческими текстами'''
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk

import os
import configparser

def destroy_cb(widget):
    Gtk.main_quit()
    return False

class Popup:
    def __init__(self, text=None):
#        self.title = ""
        self.text = text
        dialog = Gtk.Dialog(title='warning')
        label = Gtk.Label()
        label.set_text(self.text)
        dialog.vbox.pack_start(label, True, True, 10)
        label.show()
        dialog.show()



class Options:
    '''сначала читаем из файла конфиг, отдаем в gui, потом после сохранения проверяем изменения? Или просто пишем все, что есть'''

    def __init__(self, gui):
        
        self.gui = gui

        self.conf_path = os.path.join(os.path.expanduser('~'), '.config', 'hiptools', 'hiptoolsrc')
        #self.conf_path = os.path.join(os.path.expanduser('~'), 'hiptools', 'hiptoolsrc')

        self.sect_dict = self.read_conf()
        self.set_to_gui(self.sect_dict)

        self.gui.window2.connect('key_press_event', self.write_conf)

        self.gui.combo1.connect('changed', self.combo_on, 2)
        self.gui.combo2.connect('changed', self.combo_on, 3)

        self.gui.f_select1.connect('font-set', self.font_cb, 4)
        self.gui.f_select2.connect('font-set', self.font_cb, 5)
        self.gui.f_select3.connect('font-set', self.font_cb, 6)
        self.gui.ch_but1.connect('toggled', self.ch_but_on, 7)
        self.gui.ch_but2.connect('toggled', self.ch_but_on, 8)
        self.gui.ch_but3.connect('toggled', self.ch_but_on, 9)
        self.gui.ch_but4.connect('toggled', self.ch_but_on, 10)

        self.gui.combo3.connect('changed', self.combo_on, 11)
        self.pairs = (("LibraryPaths", "gr_path"), 
                      ("LibraryPaths", "sl_path"), 
                      ("SearchOptions", "default_search_group_gr"), 
                      ("SearchOptions", "default_search_group"),
                      ("Fonts", "gr_font"),
                      ("Fonts", "sl_font"),
                      ("Fonts", "reg_font"),
                      ("SearchOptions", "diacritics_off"),
                      ("SearchOptions", "betacode"),
                      ("SearchOptions", "punct_del"),
                      ("Style", "brackets_off"),
                      ("Style", "default_style")
                     )  # Нужно для записи по номеру события. Номер элемента соотв номеру события (виджета). 
        self.events = []  # счетчик событий. При записи в файл 
                          # будут обновлятся только соотв. записи

    def font_cb(self, f_button, num):
        '''Callback to font-select dialog'''
        if not num in self.events:
            self.events.append(num)  # не дублируем события из одного виджета, важен факт

        font = f_button.get_font_name()
        if num == 4:
            self.sect_dict['Fonts']['gr_font'] = font
        elif num == 5:
            self.sect_dict['Fonts']['sl_font'] = font
        elif num == 6:
            self.sect_dict['Fonts']['reg_font'] = font

    def combo_on(self, widget, num):
        '''Callback for all the ComboBoxes'''
        if not num in self.events:
            self.events.append(num)  # не дублируем события из одного виджета, важен факт
        name = widget.get_active_text() 
        if num == 2:
            self.sect_dict['SearchOptions']['default_search_group_gr'] = name
        elif num == 3:
            self.sect_dict['SearchOptions']['default_search_group'] = name
        elif num == 11:
            self.sect_dict['Style']['default_style'] = name

    def ch_but_on(self, button, num):
        '''Callback for all the CheckButtons'''
        if not num in self.events:
            self.events.append(num)  # не дублируем события из одного виджета, важен факт
        name = str(button.get_active())
        print(name)
        if num == 7:
            self.sect_dict['SearchOptions']['diacritics_off'] = name
        elif num == 8:
            self.sect_dict['SearchOptions']['betacode'] = name
        elif num == 9:
            self.sect_dict['SearchOptions']['punct_del'] = name
        elif num == 10:
            self.sect_dict['Style']['brackets_off'] = name

    def read_conf(self):
        self.config = configparser.ConfigParser()
        self.config.read(self.conf_path)
        
        sect_dict = {}
        # каждая секция (элемент в списке парсера) это словарь опция=значение
        for sect in self.config.sections():
            sect_dict[sect] = dict(self.config.items(sect))
            
        return sect_dict
    
    def set_to_gui(self, sect_dict):
        self.gui.entry1.set_text(sect_dict["LibraryPaths"]["gr_path"])
        self.gui.entry2.set_text(sect_dict["LibraryPaths"]["sl_path"])
        # set text в первый комбо-бокс
        conf_opt = sect_dict["SearchOptions"]["default_search_group_gr"]
        for i in range(len(self.gui.combo_lst_gr)):
            if self.gui.combo_lst_gr[i] == conf_opt:
                self.gui.combo1.set_active(i)

        # set text во второй комбо-бокс
        conf_opt = sect_dict["SearchOptions"]["default_search_group"]
        for i in range(len(self.gui.combo_lst_sl)):
            if self.gui.combo_lst_sl[i] == conf_opt:
                self.gui.combo2.set_active(i)

        arg = sect_dict["SearchOptions"]["diacritics_off"]
        if arg == "True":
            self.gui.ch_but1.set_active(1)
        else:
            self.gui.ch_but1.set_active(0)

        arg = sect_dict["SearchOptions"]["betacode"]
        if arg == "True":
            self.gui.ch_but2.set_active(1)
        else:
            self.gui.ch_but2.set_active(0)

        arg = sect_dict["SearchOptions"]["punct_del"]
        if arg == "True":
            self.gui.ch_but3.set_active(1)
        else:
            self.gui.ch_but3.set_active(0)

        arg = sect_dict["Style"]["brackets_off"]
        if arg == "True":
            self.gui.ch_but4.set_active(1)
        else:
            self.gui.ch_but4.set_active(0)

        self.gui.f_select1.set_font_name(sect_dict["Fonts"]["gr_font"])

        self.gui.f_select2.set_font_name(sect_dict["Fonts"]["sl_font"])

        self.gui.f_select3.set_font_name(sect_dict["Fonts"]["reg_font"])

        if sect_dict["Style"]["default_style"] == "slavonic":
            self.gui.combo3.set_active(0) # slavonic
        else:
            self.gui.combo3.set_active(1)  # hip 

    def write_conf(self, widget, event):

        keyname = Gdk.keyval_name(event.keyval)
        # запишем измененные данные в файл

        if (keyname == "s" or keyname == "Cyrillic_yeru") and event.get_state() & Gdk.ModifierType.CONTROL_MASK:
            for n in self.events:
                opt1, opt2 = self.pairs[n]
                print(n, opt1, opt2)
                self.config.set(opt1, opt2, self.sect_dict[opt1][opt2])

            with open(self.conf_path, "w") as fp:
                self.config.write(fp)
            fp.close()

            Popup("Записал")

class GuiOpt:
    '''Gui для отображения данных конфигурации'''
    def __init__(self):

        self.combo_lst_sl = ['Богослужебные', 'Свящ. Писание', 'Все книги', 'Четьи книги', 'Акафисты', 'Алфавит', 'Апостол', 'Часослов', 'Треодь цветная', 'Добротолюбие', 'Экзапостилларии', 'Евангелия', 'Ифика', 'Каноны', 'Молитвы на молебнах', 'Общая минея', 'Октоих', 'Пролог', 'Книга правил', 'Псалтырь', 'Треодь постная', 'Служебник', 'Типикон', 'Требник', 'Ветхий Завет', 'Минеи месячные']
        self.combo_lst_gr = ['All_services', 'All_read', 'Minologion_base', 'Minologion extra', 'Minologion all', 'Euhologion', 'Hieratikon', 'Octoechos', 'Orologion', 'Penticostartion', 'Triodion', 'Agia_grafh', 'Psalthrion', 'Zlat_Mat', 'Zlat_John']

        self.window2 = Gtk.Window()
        self.window2.set_resizable(True)
        self.window2.set_size_request(950, 450)

        self.window2.set_title("Просмотр результатов")
        self.window2.set_border_width(0)

        self.note = Gtk.Notebook()

        self.window2.add(self.note)
        self.note.set_scrollable(True)
        self.show_tabs = True
        self.show_border = True

        self.page1 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        self.page1.set_border_width(10)
        
        label_main1 = Gtk.Label() # просто затычка, так красивее
        self.page1.add(label_main1)

        hbox1 = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        self.page1.add(hbox1)

        self.label2 = Gtk.Label()
        self.label2.set_text("Путь к греческой библиотеке")   
 
        hbox1.pack_start(self.label2, False, False, 10)
        # gr_path 
        self.entry1 = Gtk.Entry()
        hbox1.pack_start(self.entry1, True, True, 10)

        hbox2 = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        self.page1.add(hbox2)

        self.label3 = Gtk.Label()
        self.label3.set_text("Путь к славянской библиотеке")   
 
        hbox2.pack_start(self.label3, False, False, 10)
        # sl_path 
        self.entry2 = Gtk.Entry()
        hbox2.pack_start(self.entry2, True, True, 10)

        self.page2 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        self.page2.set_border_width(10)
 
        label_main2 = Gtk.Label()
        self.page2.add(label_main2)

        hbox3 = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        self.page2.add(hbox3)

        self.label4 = Gtk.Label()
        self.label4.set_text("Группа поиска в греческом по умолчанию")
 
        hbox3.pack_start(self.label4, False, False, 10)
        # default_search_group_gr 
        self.combo1 = Gtk.ComboBoxText()
        hbox3.pack_start(self.combo1, False, False, 10)
        for i in self.combo_lst_gr: 
            self.combo1.append_text(i.strip())

        hbox4 = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        self.page2.add(hbox4)

        self.label5 = Gtk.Label()
        self.label5.set_text("Группа поиска в славянском по умолчанию")

        hbox4.pack_start(self.label5, False, False, 10)
        # default_search_group 
        self.combo2 = Gtk.ComboBoxText()
        hbox4.pack_start(self.combo2, False, False, 10)

        for i in self.combo_lst_sl: 
            self.combo2.append_text(i.strip())

        label_main3 = Gtk.Label()
        self.page2.add(label_main3)  # разделитель, красивее

        hbox5 = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        self.page2.add(hbox5)

        self.label6 = Gtk.Label()
        self.label6.set_text("Отключить диакритику по умолчанию")

        hbox5.pack_start(self.label6, False, False, 10)
        # diacritics_off 
        self.ch_but1 = Gtk.CheckButton()
        hbox5.pack_start(self.ch_but1, False, False, 10)

        hbox6 = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        self.page2.add(hbox6)

        self.label7 = Gtk.Label()
        self.label7.set_text("Включить betacode при наборе")

        hbox6.pack_start(self.label7, False, False, 10)
        # betacode 
        self.ch_but2 = Gtk.CheckButton()
        hbox6.pack_start(self.ch_but2, False, False, 10)

        hbox7 = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        self.page2.add(hbox7)

        self.label8 = Gtk.Label()
        self.label8.set_text("Убрать знаки препинания при поиске")

        hbox7.pack_start(self.label8, False, False, 10)
        # punct_del 
        self.ch_but3 = Gtk.CheckButton()
        hbox7.pack_start(self.ch_but3, False, False, 10)

        self.page3 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        self.page3.set_border_width(10)

        label_main3 = Gtk.Label()
        self.page3.add(label_main3)

        hbox8 = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        self.page3.add(hbox8)

        self.label9 = Gtk.Label()
        self.label9.set_text("Греческий шрифт")

        hbox8.pack_start(self.label9, False, False, 10)
        # gr_font 
        self.f_select1 = Gtk.FontButton()

        hbox8.pack_start(self.f_select1, True, True, 10)

        hbox9 = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        self.page3.add(hbox9)

        self.label10 = Gtk.Label()
        self.label10.set_text("Славянский шрифт")

        hbox9.pack_start(self.label10, False, False, 10)
        # sl_font 
        self.f_select2 = Gtk.FontButton()
        hbox9.pack_start(self.f_select2, True, True, 10)

        hbox10 = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        self.page3.add(hbox10)

        self.label11 = Gtk.Label()
        self.label11.set_text("Обычный шрифт")

        hbox10.pack_start(self.label11, False, False, 10)
        # reg_font 
        self.f_select3 = Gtk.FontButton()
        hbox10.pack_start(self.f_select3, True, True, 10)

        self.page4 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        self.page4.set_border_width(10)

        label_main4 = Gtk.Label()
        self.page4.add(label_main4)

        hbox11 = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        self.page4.add(hbox11)

        self.label12 = Gtk.Label()
        self.label12.set_text("Стиль по умолчанию")

        hbox11.pack_start(self.label12, False, False, 10)
        # default_style 
        self.combo3 = Gtk.ComboBoxText()
        hbox11.pack_start(self.combo3, False, False, 10)
        for i in ["slavonic", "plain"]:
            self.combo3.append_text(i.strip())

        hbox12 = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        self.page4.add(hbox12)

        self.label13 = Gtk.Label()
        self.label13.set_text("Отключить комментарии")

        hbox12.pack_start(self.label13, False, False, 10)
        # brackets_off 
        self.ch_but4 = Gtk.CheckButton()
        hbox12.pack_start(self.ch_but4, False, False, 10)

        self.note.append_page(self.page2, Gtk.Label(label="Опции поиска"))
        self.note.append_page(self.page3, Gtk.Label(label="Шрифты"))
        self.note.append_page(self.page1, Gtk.Label(label="Пути"))
        self.note.append_page(self.page4, Gtk.Label(label="Отображение hip-текста"))

        self.window2.show_all()

        if __name__ == '__main__':
            self.window2.connect('destroy', destroy_cb)

def main():
    Gtk.main()
    return 0

if __name__ == '__main__':

    opt = Options(GuiOpt())
    
    main()

'''{'LibraryPaths': {'gr_path': '/usr/local/lib/hiptools/greeklib', 'sl_path': '/usr/local/lib/hiptools/hiplib'}, 'SearchOptions': {'default_search_group': 'Богослужебные', 'default_search_group_gr': 'Minologion_base', 'diacritics_off': 'True', 'betacode': 'True', 'punct_del': 'True'}, 'Fonts': {'gr_font': 'Old Standard TT 24', 'sl_font': 'Hirmos Ponomar 24', 'reg_font': 'Dejavu Sans 14'}, 'Style': {'default_style': 'slavonic', 'brackets_off': 'True'}}'''

# TODO: 
# 1. Всплывающее меню "Записали"
# 2. Кнопка Save на всех страницах notebook'а
# 3. Вертикальные боксы для выравнивания виджетов
# 4. Диалог открытия файла вместо Entry для путей к библиотекам
